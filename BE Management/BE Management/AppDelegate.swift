//
//  AppDelegate.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit
import IQKeyboardManagerSwift
import SJSwiftSideMenuController

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        return true
    }

    func setRootViewController() {
        let mainVC = SJSwiftSideMenuController()
        if let homeVC = HomeViewController.instance() {
            if let leftVC = SideMenuViewController.instance() {
                SJSwiftSideMenuController.setUpNavigation(rootController: homeVC, leftMenuController: leftVC, rightMenuController: nil, leftMenuType: .SlideOver, rightMenuType: .SlideOver)
                SJSwiftSideMenuController.enableDimbackground = true
                SJSwiftSideMenuController.enableSwipeGestureWithMenuSide(menuSide: .NONE)
                if  UIScreen.main.bounds.size.width > UIScreen.main.bounds.size.height {
                    SJSwiftSideMenuController.leftMenuWidth = UIScreen.main.bounds.height * 0.8
                }else {
                    SJSwiftSideMenuController.leftMenuWidth = UIScreen.main.bounds.width * 0.8
                }
               
                self.window?.rootViewController = mainVC
            }
        }
        
    }

}

