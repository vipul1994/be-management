//
//  BudgetFollowUpViewController.swift
//  BE Management
//
//  Created by Vipul  on 15/05/23.
//

import UIKit
import MSCircularSlider

class BudgetFollowUpViewController: BaseViewController {
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var featureCollection: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var spendAmountLabel: UILabel!
    @IBOutlet weak var remainingAmountLabel: UILabel!
    @IBOutlet weak var progressView: MSCircularSlider!
    
    var cellHeightWidth = 0.0
    var collectionArray = ["Expanse Management","Budget Follow-up","Budget Rebalancing","Create Another Budget","Expanse Management","Budget Follow-up","Budget Rebalancing","Create Another Budget","Expanse Management","Budget Follow-up","Budget Rebalancing","Create Another Budget","Expanse Management","Budget Follow-up"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> BudgetFollowUpViewController? {
        return UIStoryboard(name: IdentifierConstant.budgetFollowUp, bundle: nil).instantiateViewController(withIdentifier: BudgetFollowUpViewController.identifier) as? BudgetFollowUpViewController
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if isIphone() {
            if isLandscapeOrientation() {
                cellHeightWidth = (size.height - 80) / 1.5
                if collectionArray.count % 3 == 0 {
                    collectionHeight.constant = cellHeightWidth * CGFloat(collectionArray.count / 3)
                }else {
                    collectionHeight.constant = cellHeightWidth * CGFloat((collectionArray.count / 3) + 1)
                }
            } else {
                collectionHeight.constant = ((size.width - 40)) * CGFloat(collectionArray.count.isMultiple(of: 2) ? (collectionArray.count / 4) : ((collectionArray.count / 4) + 1))
                cellHeightWidth = (size.width - 40) / 2
            }
        }else {
            cellHeightWidth = (size.width - 80.0) / 3
            if collectionArray.count % 3 == 0 {
                collectionHeight.constant = cellHeightWidth * CGFloat(collectionArray.count / 3)
            }else {
                collectionHeight.constant = cellHeightWidth * CGFloat((collectionArray.count / 3) + 1)
            }
        }
        featureCollection.reloadData()
    }
    
    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.budgetFollowUp))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
        if isIphone() {
            if isLandscapeOrientation() {
                cellHeightWidth = ((view.frame.height - 80.0) / 1.5)
                if collectionArray.count % 3 == 0 {
                    collectionHeight.constant = cellHeightWidth * CGFloat(collectionArray.count / 3)
                }else {
                    collectionHeight.constant = cellHeightWidth * CGFloat((collectionArray.count / 3) + 1)
                }
            } else {
                collectionHeight.constant = ((view.frame.width - 40)) * CGFloat(collectionArray.count.isMultiple(of: 2) ? (collectionArray.count / 4) : ((collectionArray.count / 4) + 1))
                cellHeightWidth = (view.frame.width - 40.0) / 2
            }
        }else {
            cellHeightWidth = (view.frame.width - 80.0) / 3
            if collectionArray.count % 3 == 0 {
                collectionHeight.constant = cellHeightWidth * CGFloat(collectionArray.count / 3)
            }else {
                collectionHeight.constant = cellHeightWidth * CGFloat((collectionArray.count / 3) + 1)
            }
        }
        featureCollection.reloadData()
    }
    
    @IBAction func leftAction(_ sender: Any) {
    }
    
    @IBAction func rightAction(_ sender: Any) {
    }
}


extension BudgetFollowUpViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BudgetFollowUpCollectionViewCell.identifier, for: indexPath) as? BudgetFollowUpCollectionViewCell else {return UICollectionViewCell()}
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellHeightWidth, height: cellHeightWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("")
    }
    
}
