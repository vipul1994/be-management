//
//  SignInViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class SignInViewController: BaseViewController {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if self.isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    class func instance() -> SignInViewController? {
        return UIStoryboard(name: IdentifierConstant.signIn, bundle: nil).instantiateViewController(withIdentifier: SignInViewController.identifier) as? SignInViewController
    }

    private func setUI() {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.signIn,isLeftBarShow: false))
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if let pinCodeVC = PincodeViewController.instance() {
            self.navigationController?.pushViewController(pinCodeVC, animated: true)
        }
    }
}
