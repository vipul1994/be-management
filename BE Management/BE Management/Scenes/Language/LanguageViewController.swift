//
//  LanguageViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class LanguageViewController: BaseViewController {

    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var languageTable: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    var isFromSideMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> LanguageViewController? {
        return UIStoryboard(name: IdentifierConstant.language, bundle: nil).instantiateViewController(withIdentifier: LanguageViewController.identifier) as? LanguageViewController
    }
    
    
    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.language,isSideMenu: isFromSideMenu))
        navigationView.leftBarClouser = {
            if self.isFromSideMenu {
                self.showSideMenu()
            }else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

}

extension LanguageViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LanguageTableViewCell.identifier, for: indexPath) as? LanguageTableViewCell else { return UITableViewCell()}
        return cell
    }
    
}
