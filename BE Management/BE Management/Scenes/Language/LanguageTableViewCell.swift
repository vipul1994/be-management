//
//  LanguageTableViewCell.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class LanguageTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectionImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
