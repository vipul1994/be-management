//
//  ConfirmPasswordViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class ConfirmPasswordViewController: BaseViewController {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var passwordMatchImage: UIImageView!
    @IBOutlet weak var passwordEyeImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    class func instance() -> ConfirmPasswordViewController? {
        return UIStoryboard(name: IdentifierConstant.password, bundle: nil).instantiateViewController(withIdentifier: ConfirmPasswordViewController.identifier) as? ConfirmPasswordViewController
    }

    private func setUI() {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.confirmPassword))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if let pinCodeVC = ProfileViewController.instance() {
            self.navigationController?.pushViewController(pinCodeVC, animated: true)
        }
    }

    @IBAction func passwordHideShowAction(_ sender: Any) {
        passwordEyeImage.image = confirmPasswordTextField.isSecureTextEntry ? UIImage(named: "img_hide_password") : UIImage(named: "img_show_password")
        confirmPasswordTextField.isSecureTextEntry = !confirmPasswordTextField.isSecureTextEntry
    }
}

extension ConfirmPasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //set logic for set image tick for validation
        return true
    }
}
