//
//  PasswordViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class PasswordViewController: BaseViewController {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var mimumumCharacterImage: UIImageView!
    @IBOutlet weak var oneUppercaseImage: UIImageView!
    @IBOutlet weak var oneLowercaseImage: UIImageView!
    @IBOutlet weak var oneNumberImage: UIImageView!
    @IBOutlet weak var passwordEyeImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    class func instance() -> PasswordViewController? {
        return UIStoryboard(name: IdentifierConstant.password, bundle: nil).instantiateViewController(withIdentifier: PasswordViewController.identifier) as? PasswordViewController
    }

    private func setUI() {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.password))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if let pinCodeVC = ConfirmPasswordViewController.instance() {
            self.navigationController?.pushViewController(pinCodeVC, animated: true)
        }
    }
    
    @IBAction func passwordHideShowAction(_ sender: Any) {
        passwordEyeImage.image = passwordTextField.isSecureTextEntry ? UIImage(named: "img_hide_password") : UIImage(named: "img_show_password")
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
    }
}

extension PasswordViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //set logic for set image tick for validation
        return true
    }
}
