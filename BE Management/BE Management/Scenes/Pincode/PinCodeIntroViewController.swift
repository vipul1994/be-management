//
//  PinCodeIntroViewController.swift
//  BE Management
//
//  Created by Vipul  on 13/05/23.
//

import UIKit

class PinCodeIntroViewController: BaseViewController {
    
    @IBOutlet weak var navigationView: NavigationView!
    var isFromSideMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> PinCodeIntroViewController? {
        return UIStoryboard(name: IdentifierConstant.pinCode, bundle: nil).instantiateViewController(withIdentifier: PinCodeIntroViewController.identifier) as? PinCodeIntroViewController
    }

    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.pinCode))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        if let pinCodePinVC = PincodeViewController.instance() {
            pinCodePinVC.isFromSideMenu = true
            self.navigationController?.pushViewController(pinCodePinVC, animated: true)
        }
    }
}
