//
//  ConfirmPincodeViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit
import SVPinView

class ConfirmPincodeViewController: BaseViewController {

    @IBOutlet weak var pinView: SVPinView!
    @IBOutlet weak var navigationView: NavigationView!
    var isFromSideMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> ConfirmPincodeViewController? {
        return UIStoryboard(name: IdentifierConstant.pinCode, bundle: nil).instantiateViewController(withIdentifier: ConfirmPincodeViewController.identifier) as? ConfirmPincodeViewController
    }

    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.pinCode))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
        pinView.style = .box
        pinView.borderLineColor = .clear
    }

    private func redirectToPasswordViewController() {
        if let passwordVC = PasswordViewController.instance() {
            self.navigationController?.pushViewController(passwordVC, animated: true)
        }
    }
    
    private func redirectToHomeViewController() {
        if let alertVC = AlertViewController.instance() {
            alertVC.model = AlertModel(image: UIImage(named: "img_alert_success"), type: AlertType.sideMenuPinSet, title: "", description: "Your PIN has been set set successfully!", firstButtonTitle: "Back to Home", secondButtonTitle: nil)
            alertVC.modalPresentationStyle = .overCurrentContext
            alertVC.firstButtonClouser = { type in
                self.navigationController?.popToRootViewController(animated: true)
            }
            self.present(alertVC, animated: false)
        }
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        isFromSideMenu ? redirectToHomeViewController() : redirectToPasswordViewController()
    }
}

