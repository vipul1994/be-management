//
//  PincodeViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit
import SVPinView

class PincodeViewController: BaseViewController {

    @IBOutlet weak var pinView: SVPinView!
    @IBOutlet weak var navigationView: NavigationView!
    
    var isFromSideMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> PincodeViewController? {
        return UIStoryboard(name: IdentifierConstant.pinCode, bundle: nil).instantiateViewController(withIdentifier: PincodeViewController.identifier) as? PincodeViewController
    }

    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.pinCode))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
        pinView.style = .box
        pinView.font = isIphone() ? UIFont.systemFont(ofSize: 15) : UIFont.systemFont(ofSize: 25)
        pinView.borderLineColor = .clear
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        if let confirmPinVC = ConfirmPincodeViewController.instance() {
            confirmPinVC.isFromSideMenu = isFromSideMenu
            self.navigationController?.pushViewController(confirmPinVC, animated: true)
        }
    }
}
