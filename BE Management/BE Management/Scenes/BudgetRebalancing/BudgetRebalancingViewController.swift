//
//  BudgetRebalancingViewController.swift
//  BE Management
//
//  Created by Vipul  on 15/05/23.
//

import UIKit
import FittedSheets

class BudgetRebalancingViewController: BaseViewController {
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var featureCollection: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    var cellHeightWidth = 0.0
    var collectionArray = ["","","","","","","","","","",""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> BudgetRebalancingViewController? {
        return UIStoryboard(name: IdentifierConstant.budgetRebalancing, bundle: nil).instantiateViewController(withIdentifier: BudgetRebalancingViewController.identifier) as? BudgetRebalancingViewController
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if isIphone() {
            if isLandscapeOrientation() {
                cellHeightWidth = (size.height - 80) / 1.5
                if collectionArray.count % 3 == 0 {
                    collectionHeight.constant = (cellHeightWidth * 0.8) * CGFloat(collectionArray.count / 3)
                }else {
                    collectionHeight.constant = (cellHeightWidth * 0.8) * CGFloat((collectionArray.count / 3) + 1)
                }
            } else {
                collectionHeight.constant = (((size.width - 40)) * CGFloat(collectionArray.count.isMultiple(of: 2) ? (collectionArray.count / 4) : ((collectionArray.count / 4) + 1))) * 0.8
                cellHeightWidth = (size.width - 40) / 2
            }
        }else {
            cellHeightWidth = (size.width - 40.0) / 3
            if collectionArray.count % 3 == 0 {
                collectionHeight.constant = cellHeightWidth * CGFloat(collectionArray.count / 3)
            }else {
                collectionHeight.constant = cellHeightWidth * CGFloat((collectionArray.count / 3) + 1)
            }
        }
        featureCollection.reloadData()
    }
    
    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.budgetRebalancing))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
        if isIphone() {
            if isLandscapeOrientation() {
                cellHeightWidth = ((view.frame.height - 80.0) / 1.5)
                if collectionArray.count % 3 == 0 {
                    collectionHeight.constant = (cellHeightWidth * 0.8) * CGFloat(collectionArray.count / 3)
                }else {
                    collectionHeight.constant = (cellHeightWidth * 0.8) * CGFloat((collectionArray.count / 3) + 1)
                }
            } else {
                collectionHeight.constant = (((view.frame.width - 40)) * CGFloat(collectionArray.count.isMultiple(of: 2) ? (collectionArray.count / 4) : ((collectionArray.count / 4) + 1))) * 0.8
                cellHeightWidth = (view.frame.width - 40.0) / 2
            }
        }else {
            cellHeightWidth = (view.frame.width - 40.0) / 3
            if collectionArray.count % 3 == 0 {
                collectionHeight.constant = cellHeightWidth * CGFloat(collectionArray.count / 3)
            }else {
                collectionHeight.constant = cellHeightWidth * CGFloat((collectionArray.count / 3) + 1)
            }
        }
        featureCollection.reloadData()
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if let savingVC = BudgetRebalancingConfirmViewController.instance() {
            var height = 380.0
            if isIphone() {
                height = 320.0
            }
            let sheetController = SheetViewController(
                controller: savingVC,
                sizes: [.fixed(height)])
            sheetController.autoAdjustToKeyboard = true
            sheetController.allowPullingPastMaxHeight = false
            self.present(sheetController, animated: false, completion: nil)
        }
    }
}


extension BudgetRebalancingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BudgetRebalancingCollectionViewCell.identifier, for: indexPath) as? BudgetRebalancingCollectionViewCell else {return UICollectionViewCell()}
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellHeightWidth, height: cellHeightWidth * 0.8)
    }
}
