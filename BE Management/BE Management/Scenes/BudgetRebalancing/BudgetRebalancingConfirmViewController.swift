//
//  BudgetRebalancingConfirmViewController.swift
//  BE Management
//
//  Created by Vipul  on 15/05/23.
//

import UIKit

class BudgetRebalancingConfirmViewController: BaseViewController {
    
    @IBOutlet weak var budgetTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    class func instance() -> BudgetRebalancingConfirmViewController? {
        return UIStoryboard(name: IdentifierConstant.budgetRebalancing, bundle: nil).instantiateViewController(withIdentifier: BudgetRebalancingConfirmViewController.identifier) as? BudgetRebalancingConfirmViewController
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        if let alertVC = AlertViewController.instance() {
            alertVC.model = AlertModel(image: UIImage(named: "img_alert_success"), type: AlertType.homeSupport, title: "Congratulation ", description: "You have successfully updated the budgett", firstButtonTitle: "Continue", secondButtonTitle: nil)
            alertVC.modalPresentationStyle = .overCurrentContext
            alertVC.firstButtonClouser = { type in
                self.navigationController?.popViewController(animated: true)
            }
            self.present(alertVC, animated: false)
        }
        
    }
    
}
