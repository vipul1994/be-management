//
//  BudgetRebalancingCollectionViewCell.swift
//  BE Management
//
//  Created by Vipul  on 15/05/23.
//

import UIKit

class BudgetRebalancingCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBG.setCornerRadiusAndShadow()
    }
}
