//
//  SavingsViewController.swift
//  BE Management
//
//  Created by Vipul  on 14/05/23.
//

import UIKit

class SavingsViewController: BaseViewController {

    @IBOutlet weak var savingTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    class func instance() -> SavingsViewController? {
        return UIStoryboard(name: IdentifierConstant.savings, bundle: nil).instantiateViewController(withIdentifier: SavingsViewController.identifier) as? SavingsViewController
    }
    
}
