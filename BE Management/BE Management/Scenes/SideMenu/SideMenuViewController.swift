//
//  SideMenuViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit
import SJSwiftSideMenuController
import FittedSheets

class SideMenuViewController: BaseViewController {
    
    var array = ["Home","Currency","Language","Saving","Select budget","PIN code","Budget history & Receipts","Privacy","Delete Data"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    class func instance() -> SideMenuViewController? {
        return UIStoryboard(name: IdentifierConstant.sideMenu, bundle: nil).instantiateViewController(withIdentifier: SideMenuViewController.identifier) as? SideMenuViewController
    }
    
    private func isLastIndex(row: Int) -> Bool {
        (array.count - 1) == row
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        SJSwiftSideMenuController.hideLeftMenu()
    }
    
    @IBAction func premiumAction(_ sender: Any) {
        SJSwiftSideMenuController.hideLeftMenu()
        if let premiumVC = PremiumViewController.instance() {
            SJSwiftSideMenuController.pushViewController(premiumVC, animated: true)
        }
    }
    
    @IBAction func profileAction(_ sender: Any) {
        SJSwiftSideMenuController.hideLeftMenu()
        if let editProfileVC = EditProfileViewController.instance() {
            SJSwiftSideMenuController.pushViewController(editProfileVC, animated: true)
        }
    }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuTableViewCell.identifier, for: indexPath) as? SideMenuTableViewCell else { return UITableViewCell()}
        cell.titleLabel.text = array[indexPath.row]
        if isLastIndex(row: indexPath.row) {
            cell.titleLabel.textColor = .red
            cell.sepratorImage.isHidden = true
        }else {
            cell.titleLabel.textColor = .black
            cell.sepratorImage.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SJSwiftSideMenuController.hideLeftMenu()
        switch indexPath.row {
        case 0:
            if let homeVC = HomeViewController.instance() {
                SJSwiftSideMenuController.pushViewController(homeVC, animated: true)
            }
        case 1:
            if let currencyVC = CurrencyViewController.instance() {
                currencyVC.isFromSideMenu = true
                SJSwiftSideMenuController.pushViewController(currencyVC, animated: true)
            }
        case 2:
            if let languageVC = LanguageViewController.instance() {
                languageVC.isFromSideMenu = true
                SJSwiftSideMenuController.pushViewController(languageVC, animated: true)
            }
        case 3:
            if let savingVC = SavingsViewController.instance() {
                var height = 340.0
                if isIphone() {
                    height = 260.0
                }
                let sheetController = SheetViewController(
                    controller: savingVC,
                    sizes: [.fixed(height)])
                sheetController.autoAdjustToKeyboard = true
                sheetController.allowPullingPastMaxHeight = false
                self.present(sheetController, animated: false, completion: nil)
            }
            
        case 4:
            if let createBudgetVC = CreateBudgetIntroViewController.instance() {
                SJSwiftSideMenuController.pushViewController(createBudgetVC, animated: true)
            }
        case 5:
            if let pinCodeVC = PinCodeIntroViewController.instance() {
                pinCodeVC.isFromSideMenu = true
                SJSwiftSideMenuController.pushViewController(pinCodeVC, animated: true)
            }
        case 6:
            if let pinCodeVC = BudgetHistoryViewController.instance() {
                SJSwiftSideMenuController.pushViewController(pinCodeVC, animated: true)
            }
        case 7:
            if let alertVC = AlertViewController.instance() {
                alertVC.model = AlertModel(image: UIImage(named: "img_alert_privacy"), type: AlertType.sideMenuPrivacy, title: "Privacy policy", description: "Go to the link : https://financewalletapp.com/doc s/privacy_policy.html", firstButtonTitle: "Redirect", secondButtonTitle: nil)
                alertVC.modalPresentationStyle = .overCurrentContext
                alertVC.firstButtonClouser = { type in
                    
                }
                self.present(alertVC, animated: false)
            }
            
        case 8:
            if let alertVC = AlertViewController.instance() {
                alertVC.model = AlertModel(image: UIImage(named: "img_alert_delete"), type: AlertType.sideMenuDelete, title: "Delete data?", description: "Only data from this device will be deleted. if you want to delete your account from the server, access the profile page", firstButtonTitle: "Profile page", secondButtonTitle: "Cancel")
                alertVC.modalPresentationStyle = .overCurrentContext
                alertVC.firstButtonClouser = { type in
                    
                }
                alertVC.secondButtonClouser = { type in
                    
                }
                self.present(alertVC, animated: false)
            }
        default:
            print("")
        }
    }
    
}
