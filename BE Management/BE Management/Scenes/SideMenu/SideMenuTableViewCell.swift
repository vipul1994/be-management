//
//  SideMenuTableViewCell.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sepratorImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
