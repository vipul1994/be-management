//
//  PremiumViewController.swift
//  BE Management
//
//  Created by Vipul  on 18/05/23.
//

import UIKit

class PremiumViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    class func instance() -> PremiumViewController? {
        return UIStoryboard(name: IdentifierConstant.premium, bundle: nil).instantiateViewController(withIdentifier: PremiumViewController.identifier) as? PremiumViewController
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        AppSingleton.appDelegate.setRootViewController()
    }
    
    @IBAction func oneYearAction(_ sender: Any) {
        
    }
    
    @IBAction func unlimitedAction(_ sender: Any) {
        
    }

}
