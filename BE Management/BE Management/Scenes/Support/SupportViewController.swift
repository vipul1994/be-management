//
//  SupportViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class SupportViewController: BaseViewController {
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var concernTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> SupportViewController? {
        return UIStoryboard(name: IdentifierConstant.support, bundle: nil).instantiateViewController(withIdentifier: SupportViewController.identifier) as? SupportViewController
    }
    
    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.support))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func sendAction(_ sender: Any) {
        if let alertVC = AlertViewController.instance() {
            alertVC.model = AlertModel(image: UIImage(named: "img_alert_success"), type: AlertType.homeSupport, title: "", description: "Your message has been sent. Our Support Teams will be happy to answer asap.", firstButtonTitle: "Back to Home", secondButtonTitle: nil)
            alertVC.modalPresentationStyle = .overCurrentContext
            alertVC.firstButtonClouser = { type in
                self.navigationController?.popViewController(animated: true)
            }
            self.present(alertVC, animated: false)
        }
    }
    
}
