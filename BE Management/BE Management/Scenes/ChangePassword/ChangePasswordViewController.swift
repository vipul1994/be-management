//
//  ChangePasswordViewController.swift
//  BE Management
//
//  Created by Vipul  on 14/05/23.
//

import UIKit

class ChangePasswordViewController: BaseViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var oldPasswordEyeImage: UIImageView!
    @IBOutlet weak var passwordEyeImage: UIImageView!
    @IBOutlet weak var confirmPasswordEyeImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if self.isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    class func instance() -> ChangePasswordViewController? {
        return UIStoryboard(name: IdentifierConstant.changePassword, bundle: nil).instantiateViewController(withIdentifier: ChangePasswordViewController.identifier) as? ChangePasswordViewController
    }
    
    private func setUI() {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.changePassword))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func changeAction(_ sender: Any) {
        
    }
    
    @IBAction func oldPasswordAction(_ sender: Any) {
        oldPasswordEyeImage.image = oldPasswordTextField.isSecureTextEntry ? UIImage(named: "img_hide_password") : UIImage(named: "img_show_password")
        oldPasswordTextField.isSecureTextEntry = !oldPasswordTextField.isSecureTextEntry
    }
    
    @IBAction func passwordAction(_ sender: Any) {
        passwordEyeImage.image = passwordTextField.isSecureTextEntry ? UIImage(named: "img_hide_password") : UIImage(named: "img_show_password")
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
    }
    
    @IBAction func confirmPasswordAction(_ sender: Any) {
        confirmPasswordEyeImage.image = confirmPasswordTextField.isSecureTextEntry ? UIImage(named: "img_hide_password") : UIImage(named: "img_show_password")
        confirmPasswordTextField.isSecureTextEntry = !confirmPasswordTextField.isSecureTextEntry
        
    }
}
