//
//  HomeViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit


class HomeViewController: BaseViewController {
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var featureCollection: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var monthlyBudgetLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    var cellHeightWidth = 0.0
    var collectionArray = ["Expanse Management","Budget Follow-up","Budget Rebalancing","Create Another Budget"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !GlobalUtility.shared.isAdsShown {
            if let adsVC = AdsViewController.instance() {
                adsVC.modalPresentationStyle = .overCurrentContext
                GlobalUtility.shared.isAdsShown = true
                self.present(adsVC, animated: false)
            }
        }
    }
    class func instance() -> HomeViewController? {
        return UIStoryboard(name: IdentifierConstant.home, bundle: nil).instantiateViewController(withIdentifier: HomeViewController.identifier) as? HomeViewController
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if isIphone() {
            if isLandscapeOrientation() {
                collectionHeight.constant = (size.height - 80) / 2
                cellHeightWidth = (size.height - 80) / 2
            } else {
                collectionHeight.constant = (size.width - 40)
                cellHeightWidth = (size.width - 40) / 2
            }
        }else {
            collectionHeight.constant = (size.width) / 4
            cellHeightWidth = (size.width - 80.0) / 4
        }
        featureCollection.reloadData()
    }
    
    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.pinCode,isSideMenu: true))
        navigationView.leftBarClouser = {
            self.showSideMenu()
        }
        
        if isIphone() {
            if isLandscapeOrientation() {
                collectionHeight.constant = (view.frame.height - 80.0) / 2
                cellHeightWidth = (view.frame.height - 80.0) / 2
            } else {
                collectionHeight.constant = (view.frame.width - 40)
                cellHeightWidth = (view.frame.width - 40.0) / 2
            }
        }else {
            collectionHeight.constant = (view.frame.width) / 4
            cellHeightWidth = (view.frame.width - 80.0) / 4
        }
        
        featureCollection.reloadData()
        tableViewHeight.constant = 80 * 3
    }
    
    private func getCellBackground(index: Int) -> UIImage? {
        switch index {
        case 0:
            return UIImage(named: "home_0")
        case 1:
            return UIImage(named: "home_1")
        case 2:
            return UIImage(named: "home_2")
        case 3:
            return UIImage(named: "home_3")
        default:
            return UIImage(named: "home_0")
        }
    }
    
    @IBAction func shareAction(_ sender: Any) {
        if let alertVC = AlertViewController.instance() {
            alertVC.model = AlertModel(image: UIImage(named: "img_alert_share"), type: AlertType.homeShareUs, title: "Share application", description: "I use Budget Saving and I really like it. You should try like me!", firstButtonTitle: "Copy", secondButtonTitle: nil)
            alertVC.modalPresentationStyle = .overCurrentContext
            alertVC.firstButtonClouser = { type in
                
            }
            self.present(alertVC, animated: false)
        }
    }
    
    @IBAction func evaluateAcction(_ sender: Any) {
        if let alertVC = AlertViewController.instance() {
            alertVC.model = AlertModel(image: UIImage(named: "img_alert_rateUs"), type: AlertType.homeRateUs, title: "Rate US", description: "Every day, we do our best to provide you with value-added service.", firstButtonTitle: "Go", secondButtonTitle: nil)
            alertVC.modalPresentationStyle = .overCurrentContext
            alertVC.firstButtonClouser = { type in
                
            }
            self.present(alertVC, animated: false)
        }
    }
    
    @IBAction func supportAction(_ sender: Any) {
        if let supportVC = SupportViewController.instance() {
            self.navigationController?.pushViewController(supportVC, animated: true)
        }
    }
}


extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCollectionViewCell.identifier, for: indexPath) as? HomeCollectionViewCell else {return UICollectionViewCell()}
        cell.titleLabel.text = collectionArray[indexPath.row]
        cell.iconImage.image = UIImage(named: "img_home\(indexPath.row)")
        cell.starImage.isHidden = (indexPath.row % 2 == 0)
        cell.backgroundImage.image = getCellBackground(index: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellHeightWidth, height: cellHeightWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            if let expenseManagementVC = EMViewController.instance() {
                self.navigationController?.pushViewController(expenseManagementVC, animated: true)
            }
        case 1:
            if let budgetFollowUpVC = BudgetFollowUpViewController.instance() {
                self.navigationController?.pushViewController(budgetFollowUpVC, animated: true)
            }
        case 2:
            if let budgetRebalanceVC = BudgetRebalancingViewController.instance() {
                self.navigationController?.pushViewController(budgetRebalanceVC, animated: true)
            }
        case 3:
            if let createBudgetVC = CreateBudgetIntroViewController.instance() {
                self.navigationController?.pushViewController(createBudgetVC, animated: true)
            }
        default:
            print("")
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HomeListCell.identifier, for: indexPath) as? HomeListCell else { return UITableViewCell()}
        return cell
    }
}
