//
//  HomeListCell.swift
//  BE Management
//
//  Created by Vipul  on 23/05/23.
//

import UIKit

class HomeListCell: UITableViewCell {

    @IBOutlet weak var monthYearLabel: UILabel!
    @IBOutlet weak var plannedBudgetLabel: UILabel!
    @IBOutlet weak var realBudgetLabel: UILabel!
    @IBOutlet weak var savingsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
