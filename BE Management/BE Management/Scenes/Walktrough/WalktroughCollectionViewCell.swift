//
//  WalktroughCollectionViewCell.swift
//  BE Management
//
//  Created by Vipul  on 14/05/23.
//

import UIKit

class WalktroughCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var pictureImage: UIImageView!
}
