//
//  WalktroughViewController.swift
//  BE Management
//
//  Created by Vipul  on 14/05/23.
//

import UIKit

class WalktroughViewController: BaseViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var imageCollection: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var timer = Timer()
    var currentIndex = 0
    var textCollection = [["title": "All Your Money In One Place", "description":"See all your bank account, credit cards and investment in one spot"],["title": "Know Where Your Money Goes", "description":"We have been protecting data for 30 years, We use the same technologies as your bank"],["title": "All Your Money In One Place", "description":"See all your bank account, credit cards and investment in one spot"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if self.isIphone() {
            setOrientation(stackView: stackView)
        }
        self.imageCollection.reloadData()
    }
    
    class func instance() -> WalktroughViewController? {
        return UIStoryboard(name: IdentifierConstant.walktrough, bundle: nil).instantiateViewController(withIdentifier: WalktroughViewController.identifier) as? WalktroughViewController
    }

    private func setUI() {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    private func setText(index: Int) {
        let arrayObject = textCollection[index]
        titleLabel.text = arrayObject["title"]
        titleLabel.text = arrayObject["description"]
        let indexPath = IndexPath(item: index, section: 0)
        imageCollection.isPagingEnabled = false
        imageCollection.scrollToItem(at: indexPath, at: [.centeredVertically, .centeredHorizontally], animated: true)
        imageCollection.isPagingEnabled = true
        pageControl.currentPage = index
    }
    
    @objc func timerAction() {
        if currentIndex == 2 {
            currentIndex = 0
        }else {
            currentIndex += 1
        }
        setText(index: currentIndex)
    }
    
    @IBAction func subscribeAction(_ sender: Any) {
        if let pinCodeVC = SignInViewController.instance() {
            self.navigationController?.pushViewController(pinCodeVC, animated: true)
        }
    }
    
    @IBAction func identityAction(_ sender: Any) {
        if let pinCodeVC = SignInViewController.instance() {
            self.navigationController?.pushViewController(pinCodeVC, animated: true)
        }
    }
    
    @IBAction func faceBookAction(_ sender: Any) {
    }
    
    @IBAction func twitterAction(_ sender: Any) {
    }
    
    @IBAction func gmailAction(_ sender: Any) {
    }
    
    @IBAction func appleAction(_ sender: Any) {
    }
    
}

extension WalktroughViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return textCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WalktroughCollectionViewCell.identifier, for: indexPath) as? WalktroughCollectionViewCell else {return UICollectionViewCell()}
        cell.pictureImage.image = UIImage(named: "img_walktrough_\(indexPath.row)")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: imageCollection.frame.width, height: imageCollection.frame.height)
    }
}
