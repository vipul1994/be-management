//
//  ProfileViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class ProfileViewController: BaseViewController {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var profileTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    class func instance() -> ProfileViewController? {
        return UIStoryboard(name: IdentifierConstant.profile, bundle: nil).instantiateViewController(withIdentifier: ProfileViewController.identifier) as? ProfileViewController
    }

    private func setUI() {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.profile))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        if let createBudgetVC = CreateBudgetIntroViewController.instance() {
            self.navigationController?.pushViewController(createBudgetVC, animated: true)
        }
    }
    
    @IBAction func chooseAction(_ sender: Any) {
        CustomImagePicker.shared.openImagePickerWith(sender: sender as? UIButton, mediaType: .MediaTypeImage, allowsEditing: false, actionSheetTitle: AppSingleton.appName, message: "", cancelButtonTitle: "Cancel", cameraButtonTitle: "Camera", galleryButtonTitle: "Gallery") { (_, success, dict) in
            if success {
                if let img = (dict!["image"] as? UIImage) {
                    print(img)
                }
            }
        }
    }
}
