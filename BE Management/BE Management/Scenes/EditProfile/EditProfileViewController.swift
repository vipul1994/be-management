//
//  EditProfileViewController.swift
//  BE Management
//
//  Created by Vipul  on 14/05/23.
//

import UIKit
import FittedSheets

class EditProfileViewController: BaseViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var deleteView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if self.isIphone() {
            setOrientation(stackView: stackView)
        }
        if let presentedVC = GlobalUtility.shared.currentTopViewController() as? UIAlertController {
            presentedVC.dismiss(animated: false)
        }
    }
    
    class func instance() -> EditProfileViewController? {
        return UIStoryboard(name: IdentifierConstant.editProfile, bundle: nil).instantiateViewController(withIdentifier: EditProfileViewController.identifier) as? EditProfileViewController
    }
    
    private func setUI() {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.editProfile,isSideMenu: true,isRightBarShow: true))
        navigationView.leftBarClouser = {
            self.showSideMenu()
        }
        navigationView.rightBarClouser = {
            
        }
        deleteView.layer.borderColor = UIColor.red.cgColor
        deleteView.layer.borderWidth = 1
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
    }
    
    @IBAction func editNameAction(_ sender: Any) {
        if let pinCodeVC = ChangeUserNameViewController.instance() {
            var height = 420.0
            if isIphone() {
                height = 360.0
            }
            let sheetController = SheetViewController(
                controller: pinCodeVC,
                sizes: [.fixed(height)])
            sheetController.autoAdjustToKeyboard = true
            sheetController.allowPullingPastMaxHeight = false
            self.present(sheetController, animated: false, completion: nil)
            
        }
    }
    
    @IBAction func changePasswordAction(_ sender: Any) {
        if let pinCodeVC = ChangePasswordViewController.instance() {
            self.navigationController?.pushViewController(pinCodeVC, animated: true)
        }
    }
    
    @IBAction func editProfileImageAction(_ sender: UIButton) {
        CustomImagePicker.shared.openImagePickerWith(sender:sender, mediaType: .MediaTypeImage, allowsEditing: false, actionSheetTitle: AppSingleton.appName, message: "", cancelButtonTitle: "Cancel", cameraButtonTitle: "Camera", galleryButtonTitle: "Gallery") { (_, success, dict) in
            if success {
                if let img = (dict!["image"] as? UIImage) {
                    
                }
            }
        }
    }
}
