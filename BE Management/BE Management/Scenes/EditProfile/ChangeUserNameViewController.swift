//
//  ChangeUserNameViewController.swift
//  BE Management
//
//  Created by Vipul  on 14/05/23.
//

import UIKit

class ChangeUserNameViewController: BaseViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    class func instance() -> ChangeUserNameViewController? {
        return UIStoryboard(name: IdentifierConstant.editProfile, bundle: nil).instantiateViewController(withIdentifier: ChangeUserNameViewController.identifier) as? ChangeUserNameViewController
    }
    
    @IBAction func changeAction(_ sender: Any) {
        self.dismiss(animated: false)
    }
    
}
