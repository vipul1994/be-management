//
//  CreateBudgetCategoryCollectionViewCell.swift
//  BE Management
//
//  Created by Vipul  on 16/05/23.
//

import UIKit

class CreateBudgetCategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBG.setCornerRadiusAndShadow()
    }
}
