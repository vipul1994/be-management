//
//  CreateBudgetConstitutionCollectionViewCell.swift
//  BE Management
//
//  Created by Vipul  on 16/05/23.
//

import UIKit

class CreateBudgetConstitutionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var amountTextField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBG.setCornerRadiusAndShadow()
    }
}
