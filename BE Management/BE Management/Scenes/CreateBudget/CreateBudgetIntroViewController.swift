//
//  CreateBudgetIntroViewController.swift
//  BE Management
//
//  Created by Vipul  on 16/05/23.
//

import UIKit

class CreateBudgetIntroViewController: BaseViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    class func instance() -> CreateBudgetIntroViewController? {
        return UIStoryboard(name: IdentifierConstant.createBudget, bundle: nil).instantiateViewController(withIdentifier: CreateBudgetIntroViewController.identifier) as? CreateBudgetIntroViewController
    }
    
    private func setUI() {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
        navigationView.setUpUI(NavigationViewModel(title: ""))
        navigationView.leftBarClouser = {
            self.self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func startsAction(_ sender: Any) {
        if let budgetLanguageVC = CreateBudgetLanguageViewController.instance() {
            self.navigationController?.pushViewController(budgetLanguageVC, animated: true)
        }
    }
    
}
