//
//  CreateBudgetNameViewController.swift
//  BE Management
//
//  Created by Vipul  on 16/05/23.
//

import UIKit

class CreateBudgetNameViewController:  BaseViewController {
    
    @IBOutlet weak var budgetTextField: UITextField!
    var delegate: AddBudgetName?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    class func instance() -> CreateBudgetNameViewController? {
        return UIStoryboard(name: IdentifierConstant.createBudget, bundle: nil).instantiateViewController(withIdentifier: CreateBudgetNameViewController.identifier) as? CreateBudgetNameViewController
    }
    
    @IBAction func nextAction(_ sender: Any) {
        self.dismiss(animated: false)
        delegate?.didFinish()
    }
    
}
