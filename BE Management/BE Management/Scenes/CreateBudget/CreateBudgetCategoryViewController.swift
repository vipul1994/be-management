//
//  CreateBudgetCategoryViewController.swift
//  BE Management
//
//  Created by Vipul  on 16/05/23.
//

import UIKit

class CreateBudgetCategoryViewController:  BaseViewController {
    
    var delegate: AddBudgetName?
    @IBOutlet weak var categoryText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    class func instance() -> CreateBudgetCategoryViewController? {
        return UIStoryboard(name: IdentifierConstant.createBudget, bundle: nil).instantiateViewController(withIdentifier: CreateBudgetCategoryViewController.identifier) as? CreateBudgetCategoryViewController
    }
    
    @IBAction func nextAction(_ sender: Any) {
        self.dismiss(animated: false)
    }
    
}

extension CreateBudgetCategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CreateBudgetCategoryCell.identifier, for: indexPath) as? CreateBudgetCategoryCell else {return UICollectionViewCell()}
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isIphone() {
            return CGSize(width: 40, height: 40)
        }else {
            return CGSize(width: 70, height: 70)
        }
    }
}
