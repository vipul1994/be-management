//
//  CreateBudgetLanguageViewController.swift
//  BE Management
//
//  Created by Vipul  on 16/05/23.
//

import UIKit
import FittedSheets

protocol AddBudgetName {
    func didFinish()
}

class CreateBudgetLanguageViewController: BaseViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var currencyView: UIView!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    class func instance() -> CreateBudgetLanguageViewController? {
        return UIStoryboard(name: IdentifierConstant.createBudget, bundle: nil).instantiateViewController(withIdentifier: CreateBudgetLanguageViewController.identifier) as? CreateBudgetLanguageViewController
    }
    
    private func setUI() {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
        navigationView.setUpUI(NavigationViewModel(title: ""))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
        setBorder(view: languageView)
        setBorder(view: currencyView)
    }
    private func setBorder(view: UIView) {
        view.layer.borderColor = UIColor(named: "Button")?.cgColor
        view.layer.borderWidth = 1
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if let budgetnameVC = CreateBudgetNameViewController.instance() {
            budgetnameVC.delegate = self
            var height = 420.0
            if isIphone() {
                height = 360.0
            }
            let sheetController = SheetViewController(
                controller: budgetnameVC,
                sizes: [.fixed(height)])
            sheetController.autoAdjustToKeyboard = true
            sheetController.allowPullingPastMaxHeight = false
            self.present(sheetController, animated: false, completion: nil)
            
        }
    }
    
    @IBAction func langaugeAction(_ sender: Any) {
        if let languageVC = LanguageViewController.instance() {
            self.navigationController?.pushViewController(languageVC, animated: true)
        }
    }
    
    @IBAction func currencyAction(_ sender: Any) {
        if let currencyVC = CurrencyViewController.instance() {
            self.navigationController?.pushViewController(currencyVC, animated: true)
        }
    }
}

extension CreateBudgetLanguageViewController: AddBudgetName {
    func didFinish() {
        if let createBudgetCategoryVC = CreateBudgetSetCategoryViewController.instance() {
            self.navigationController?.pushViewController(createBudgetCategoryVC, animated: true)
        }
    }
}
