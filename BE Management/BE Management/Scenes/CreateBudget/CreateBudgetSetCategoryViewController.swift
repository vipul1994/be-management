//
//  CreateBuudgetSetCategoryViewController.swift
//  BE Management
//
//  Created by Vipul  on 16/05/23.
//

import UIKit
import FittedSheets

class CreateBudgetSetCategoryViewController: BaseViewController {
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var featureCollection: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    
    var cellHeightWidth = 0.0
    var collectionArray = ["","","","","","","","","","",""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> CreateBudgetSetCategoryViewController? {
        return UIStoryboard(name: IdentifierConstant.createBudget, bundle: nil).instantiateViewController(withIdentifier: CreateBudgetSetCategoryViewController.identifier) as? CreateBudgetSetCategoryViewController
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if isIphone() {
            if isLandscapeOrientation() {
                cellHeightWidth = (size.height - 80) / 1.5
                if collectionArray.count % 3 == 0 {
                    collectionHeight.constant = (cellHeightWidth * 0.8) * CGFloat(collectionArray.count / 3)
                }else {
                    collectionHeight.constant = (cellHeightWidth * 0.8) * CGFloat((collectionArray.count / 3) + 1)
                }
            } else {
                collectionHeight.constant = (((size.width - 40)) * CGFloat(collectionArray.count.isMultiple(of: 2) ? (collectionArray.count / 4) : ((collectionArray.count / 4) + 1))) * 0.8
                cellHeightWidth = (size.width - 40) / 2
            }
        }else {
            cellHeightWidth = (size.width - 40.0) / 3
            if collectionArray.count % 3 == 0 {
                collectionHeight.constant = cellHeightWidth * CGFloat(collectionArray.count / 3)
            }else {
                collectionHeight.constant = cellHeightWidth * CGFloat((collectionArray.count / 3) + 1)
            }
        }
        featureCollection.reloadData()
    }
    
    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.setUpBudget))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
        if isIphone() {
            if isLandscapeOrientation() {
                cellHeightWidth = ((view.frame.height - 80.0) / 1.5)
                if collectionArray.count % 3 == 0 {
                    collectionHeight.constant = (cellHeightWidth * 0.8) * CGFloat(collectionArray.count / 3)
                }else {
                    collectionHeight.constant = (cellHeightWidth * 0.8) * CGFloat((collectionArray.count / 3) + 1)
                }
            } else {
                collectionHeight.constant = (((view.frame.width - 40)) * CGFloat(collectionArray.count.isMultiple(of: 2) ? (collectionArray.count / 4) : ((collectionArray.count / 4) + 1))) * 0.8
                cellHeightWidth = (view.frame.width - 40.0) / 2
            }
        }else {
            cellHeightWidth = (view.frame.width - 40.0) / 3
            if collectionArray.count % 3 == 0 {
                collectionHeight.constant = cellHeightWidth * CGFloat(collectionArray.count / 3)
            }else {
                collectionHeight.constant = cellHeightWidth * CGFloat((collectionArray.count / 3) + 1)
            }
        }
        featureCollection.reloadData()
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if let budgetSetUpVC = CreateBudgetBuildingViewController.instance() {
            self.navigationController?.pushViewController(budgetSetUpVC, animated: true)
        }
    }
    
    @IBAction func categoryAction(_ sender: Any) {
        if let budgetCategoryVC = CreateBudgetCategoryViewController.instance() {
            let sheetController = SheetViewController(
                controller: budgetCategoryVC,
                sizes: [isIphone() ? (isLandscapeOrientation() ? .percent(1.0) : .percent(0.55)) : .percent(0.85) ])
            sheetController.autoAdjustToKeyboard = true
            sheetController.allowPullingPastMaxHeight = false
            self.present(sheetController, animated: false, completion: nil)
            
        }
    }
}


extension CreateBudgetSetCategoryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CreateBudgetCategoryCollectionViewCell.identifier, for: indexPath) as? CreateBudgetCategoryCollectionViewCell else {return UICollectionViewCell()}
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellHeightWidth, height: cellHeightWidth * 0.8)
    }
}
