//
//  BudgetHistoryViewController.swift
//  BE Management
//
//  Created by Vipul  on 13/05/23.
//

import UIKit
import Charts

class BudgetHistoryViewController: BaseViewController {
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var chartView: BarChartView!
    @IBOutlet weak var tableBackgrounView: UIView!
    @IBOutlet weak var graphImage: UIImageView!
    @IBOutlet weak var graphLabel: UILabel!
    @IBOutlet weak var tableImage: UIImageView!
    @IBOutlet weak var tablLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var yearLabel: UILabel!

    lazy var formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 1
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }

    class func instance() -> BudgetHistoryViewController? {
        return UIStoryboard(name: IdentifierConstant.budgetHistory, bundle: nil).instantiateViewController(withIdentifier: BudgetHistoryViewController.identifier) as? BudgetHistoryViewController
    }
    
    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.budgetHistory,isSideMenu: true))
        navigationView.leftBarClouser = {
            self.showSideMenu()
        }
        setCornerRadius(image: graphImage)
        setCornerRadius(image: tableImage)
        setUpGraph()
    }
    
    private func setCornerRadius(image: UIImageView) {
        image.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        image.layer.cornerRadius = 15
    }

    @IBAction func graphAction(_ sender: Any) {
        graphImage.backgroundColor = UIColor(named: "Button")
        graphLabel.textColor = .white
        tableImage.backgroundColor = .clear
        tablLabel.textColor =  UIColor(named: "Button")
        scrollView.isHidden = false
        tableBackgrounView.isHidden = true
    }
    
    @IBAction func tableAction(_ sender: Any) {
        tableImage.backgroundColor = UIColor(named: "Button")
        tablLabel.textColor = .white
        graphImage.backgroundColor = .clear
        graphLabel.textColor =  UIColor(named: "Button")
        tableBackgrounView.isHidden = false
        scrollView.isHidden = true
    }
    
    @IBAction func leftAction(_ sender: Any) {
    }
    
    @IBAction func rightAction(_ sender: Any) {
    }
}

extension BudgetHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: BudgetHistoryTableViewCell.identifier, for: indexPath) as? BudgetHistoryTableViewCell else { return UITableViewCell()}
        return cell
    }
    
}

extension BudgetHistoryViewController : ChartViewDelegate {
    

    func setUpGraph() {
        chartView.delegate = self
        chartView.chartDescription.enabled = false
        chartView.maxVisibleCount = 12
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = false
        chartView.highlightFullBarEnabled = false
        
        let leftAxis = chartView.leftAxis
        leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: formatter)
        leftAxis.axisMinimum = 0
        
        
        chartView.rightAxis.enabled = false
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .top
        
        
        let l = chartView.legend
        l.horizontalAlignment = .right
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        l.form = .square
        l.formToTextSpace = 4
        l.xEntrySpace = 6
        self.setChartData(count: 12, range: 20)
    }
    
    
    func setChartData(count: Int, range: UInt32) {
        let yVals = (0..<count).map { (i) -> BarChartDataEntry in
            let mult = range
            let val1 = Double(arc4random_uniform(mult) + mult / 3)
            let val2 = Double(arc4random_uniform(mult) + mult / 3)
            let val3 = Double(arc4random_uniform(mult) + mult / 3)
            
            return BarChartDataEntry(x: Double(i), yValues: [val1, val2, val3], icon: #imageLiteral(resourceName: "img_budget_arrow"))
        }
        
        let set = BarChartDataSet(entries: yVals, label: "")
        set.drawIconsEnabled = false
        set.colors = [ChartColorTemplates.material()[0], ChartColorTemplates.material()[1], ChartColorTemplates.material()[2]]
        set.stackLabels = ["A", "B", "C"]
        
        let data = BarChartData(dataSet: set)
        data.setValueFont(.systemFont(ofSize: 7, weight: .light))
        data.setValueFormatter(DefaultValueFormatter(formatter: formatter))
        data.setValueTextColor(.white)
        
        chartView.fitBars = true
        chartView.data = data
    }
    
}
