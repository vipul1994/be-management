//
//  BudgetHistoryTableViewCell.swift
//  BE Management
//
//  Created by Vipul  on 13/05/23.
//

import UIKit

class BudgetHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var monthYearLabel: UILabel!
    @IBOutlet weak var plannedBudgetLabel: UILabel!
    @IBOutlet weak var realBudgetLabel: UILabel!
    @IBOutlet weak var savingsLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var upDownImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
