//
//  CurrencyTableViewCell.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectionImage: UIImageView!
    @IBOutlet weak var iconImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
