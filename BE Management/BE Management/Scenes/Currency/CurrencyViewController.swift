//
//  CurrencyViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class CurrencyViewController: BaseViewController {
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var currencyTable: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    var isFromSideMenu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> CurrencyViewController? {
        return UIStoryboard(name: IdentifierConstant.currency, bundle: nil).instantiateViewController(withIdentifier: CurrencyViewController.identifier) as? CurrencyViewController
    }
    
    
    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.currency,isSideMenu: isFromSideMenu))
        navigationView.leftBarClouser = {
            if self.isFromSideMenu {
                self.showSideMenu()
            }else {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}


extension CurrencyViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CurrencyTableViewCell.identifier, for: indexPath) as? CurrencyTableViewCell else { return UITableViewCell()}
        return cell
    }
    
}
