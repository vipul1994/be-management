//
//  EMSelectCategoryViewController.swift
//  BE Management
//
//  Created by Vipul  on 17/05/23.
//

import UIKit
import MSCircularSlider

class EMSelectCategoryViewController: BaseViewController {
    
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var spendAmountLabel: UILabel!
    @IBOutlet weak var remainingAmountLabel: UILabel!
    @IBOutlet weak var progressView: MSCircularSlider!
    
    var delegate: ExpenseManagementAction?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    class func instance() -> EMSelectCategoryViewController? {
        return UIStoryboard(name: IdentifierConstant.expenseManagement, bundle: nil).instantiateViewController(withIdentifier: EMSelectCategoryViewController.identifier) as? EMSelectCategoryViewController
    }
    
    @IBAction func addExpenseAction(_ sender: Any) {
        self.dismiss(animated: false)
        delegate?.redirectTo(addExpense: true)
    }
    
    @IBAction func changeExpenseAction(_ sender: Any) {
        self.dismiss(animated: false)
        delegate?.redirectTo(addExpense: false)
    }
    
}
