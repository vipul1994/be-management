//
//  EMExpenseListSelectViewController.swift
//  BE Management
//
//  Created by Vipul  on 17/05/23.
//

import UIKit

class EMExpenseListSelectViewController: BaseViewController {
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    var delegate: ExpenseManagementList?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    class func instance() -> EMExpenseListSelectViewController? {
        return UIStoryboard(name: IdentifierConstant.expenseManagement, bundle: nil).instantiateViewController(withIdentifier: EMExpenseListSelectViewController.identifier) as? EMExpenseListSelectViewController
    }
    
    @IBAction func deleteExpenseAction(_ sender: Any) {
        self.dismiss(animated: false)
        delegate?.redirectTo(delete: true)
    }
    
    @IBAction func changeExpenseAction(_ sender: Any) {
        self.dismiss(animated: false)
        delegate?.redirectTo(delete: false)
    }
    
}

