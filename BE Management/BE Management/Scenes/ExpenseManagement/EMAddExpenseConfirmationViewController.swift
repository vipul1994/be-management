//
//  EMAddExpenseConfirmationViewController.swift
//  BE Management
//
//  Created by Vipul  on 17/05/23.
//

import UIKit

class EMAddExpenseConfirmationViewController: BaseViewController {
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var houseBudgetAllocatedLabel: UILabel!
    @IBOutlet weak var consumedBudgetLabel: UILabel!
    @IBOutlet weak var remainingBudgetLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> EMAddExpenseConfirmationViewController? {
        return UIStoryboard(name: IdentifierConstant.expenseManagement, bundle: nil).instantiateViewController(withIdentifier: EMAddExpenseConfirmationViewController.identifier) as? EMAddExpenseConfirmationViewController
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if let presentedVC = GlobalUtility.shared.currentTopViewController() as? UIAlertController {
            presentedVC.dismiss(animated: false)
        }
    }
    
    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.addExpense))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func sendAction(_ sender: Any) {
        AppSingleton.appDelegate.setRootViewController()
    }
    
    @IBAction func ticcketExpnseAcction(_ sender: UIButton) {
        CustomImagePicker.shared.openImagePickerWith(sender:sender, mediaType: .MediaTypeImage, allowsEditing: false, actionSheetTitle: AppSingleton.appName, message: "", cancelButtonTitle: "Cancel", cameraButtonTitle: "Camera", galleryButtonTitle: "Gallery") { (_, success, dict) in
            if success {
                if let img = (dict!["image"] as? UIImage) {
                    
                }
            }
        }
    }
    
    @IBAction func regularExpenseAction(_ sender: UIButton) {
        CustomImagePicker.shared.openImagePickerWith(sender:sender, mediaType: .MediaTypeImage, allowsEditing: false, actionSheetTitle: AppSingleton.appName, message: "", cancelButtonTitle: "Cancel", cameraButtonTitle: "Camera", galleryButtonTitle: "Gallery") { (_, success, dict) in
            if success {
                if let img = (dict!["image"] as? UIImage) {
                    
                }
            }
        }
    }
    
}
