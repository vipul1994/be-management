//
//  EMExpenseListTableViewCell.swift
//  BE Management
//
//  Created by Vipul  on 17/05/23.
//

import UIKit

class EMExpenseListTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
