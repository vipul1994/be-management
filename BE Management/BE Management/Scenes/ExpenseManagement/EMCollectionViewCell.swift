//
//  EMCollectionViewCell.swift
//  BE Management
//
//  Created by Vipul  on 17/05/23.
//

import UIKit
import MSCircularSlider

class EMCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var spendAmountLabel: UILabel!
    @IBOutlet weak var remainingAmountLabel: UILabel!
    @IBOutlet weak var progressView: MSCircularSlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBG.setCornerRadiusAndShadow()
    }
}
