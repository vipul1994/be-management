//
//  EMExpenseListViewController.swift
//  BE Management
//
//  Created by Vipul  on 17/05/23.
//

import UIKit
import FittedSheets

protocol ExpenseManagementList {
    func redirectTo(delete: Bool)
}

class EMExpenseListViewController: BaseViewController {
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var tableBackgrounView: UIView!
    @IBOutlet weak var listTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> EMExpenseListViewController? {
        return UIStoryboard(name: IdentifierConstant.expenseManagement, bundle: nil).instantiateViewController(withIdentifier: EMExpenseListViewController.identifier) as? EMExpenseListViewController
    }
    
    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.expenseList))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension EMExpenseListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EMExpenseListTableViewCell.identifier, for: indexPath) as? EMExpenseListTableViewCell else { return UITableViewCell()}
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let EMSelectedList = EMExpenseListSelectViewController.instance() {
            EMSelectedList.delegate = self
            var height = 420.0
            if isIphone() {
                height = 330.0
            }
            let sheetController = SheetViewController(
                controller: EMSelectedList,
                sizes: [.fixed(height)])
            sheetController.autoAdjustToKeyboard = true
            sheetController.allowPullingPastMaxHeight = false
            self.present(sheetController, animated: false, completion: nil)
        }
    }
}

extension EMExpenseListViewController : ExpenseManagementList {
    func redirectTo(delete: Bool) {
        if delete {
            if let alertVC = AlertViewController.instance() {
                alertVC.model = AlertModel(image: UIImage(named: "img_alert_delete"), type: AlertType.homeRateUs, title: "Data deleted!", description: "The expense Has been deleted", firstButtonTitle: "Home", secondButtonTitle: nil)
                alertVC.modalPresentationStyle = .overCurrentContext
                alertVC.firstButtonClouser = { type in
                    AppSingleton.appDelegate.setRootViewController()
                }
                self.present(alertVC, animated: false)
            }
        }else {
            if let addExpenseVC = EMAddEditViewController.instance() {
                addExpenseVC.isFromEdit = true
                self.navigationController?.pushViewController(addExpenseVC, animated: true)
            }
        }
    }
    
    
}
