//
//  EMViewController.swift
//  BE Management
//
//  Created by Vipul  on 17/05/23.
//

import UIKit
import FittedSheets
import MSCircularSlider

protocol ExpenseManagementAction {
    func redirectTo(addExpense: Bool)
}

class EMViewController: BaseViewController {
    
    @IBOutlet weak var navigationView: NavigationView!
    @IBOutlet weak var featureCollection: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var spendAmountLabel: UILabel!
    @IBOutlet weak var remainingAmountLabel: UILabel!
    @IBOutlet weak var progressView: MSCircularSlider!
    
    var cellHeightWidth = 0.0
    var collectionArray = ["Expanse Management","Budget Follow-up","Budget Rebalancing","Create Another Budget","Expanse Management","Budget Follow-up","Budget Rebalancing","Create Another Budget","Expanse Management","Budget Follow-up","Budget Rebalancing","Create Another Budget","Expanse Management","Budget Follow-up","Budget Rebalancing"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> EMViewController? {
        return UIStoryboard(name: IdentifierConstant.expenseManagement, bundle: nil).instantiateViewController(withIdentifier: EMViewController.identifier) as? EMViewController
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if isIphone() {
            if isLandscapeOrientation() {
                cellHeightWidth = (size.height - 80) / 1.5
                if collectionArray.count % 3 == 0 {
                    collectionHeight.constant = cellHeightWidth * CGFloat(collectionArray.count / 3)
                }else {
                    collectionHeight.constant = cellHeightWidth * CGFloat((collectionArray.count / 3) + 1)
                }
            } else {
                collectionHeight.constant = ((size.width - 40)) * CGFloat(collectionArray.count.isMultiple(of: 2) ? (collectionArray.count / 4) : ((collectionArray.count / 4) + 1))
                cellHeightWidth = (size.width - 40) / 2
            }
        }else {
            cellHeightWidth = (size.width - 80.0) / 3
            if collectionArray.count % 3 == 0 {
                collectionHeight.constant = cellHeightWidth * CGFloat(collectionArray.count / 3)
            }else {
                collectionHeight.constant = cellHeightWidth * CGFloat((collectionArray.count / 3) + 1)
            }
        }
        featureCollection.reloadData()
    }
    
    private func setUI() {
        navigationView.setUpUI(NavigationViewModel(title: NavigationTitle.expenseManagement))
        navigationView.leftBarClouser = {
            self.navigationController?.popViewController(animated: true)
        }
        if isIphone() {
            if isLandscapeOrientation() {
                cellHeightWidth = ((view.frame.height - 80.0) / 1.5)
                if collectionArray.count % 3 == 0 {
                    collectionHeight.constant = cellHeightWidth * CGFloat(collectionArray.count / 3)
                }else {
                    collectionHeight.constant = cellHeightWidth * CGFloat((collectionArray.count / 3) + 1)
                }
            } else {
                collectionHeight.constant = ((view.frame.width - 40)) * CGFloat(collectionArray.count.isMultiple(of: 2) ? (collectionArray.count / 4) : ((collectionArray.count / 4) + 1))
                cellHeightWidth = (view.frame.width - 40.0) / 2
            }
        }else {
            cellHeightWidth = (view.frame.width - 80.0) / 3
            if collectionArray.count % 3 == 0 {
                collectionHeight.constant = cellHeightWidth * CGFloat(collectionArray.count / 3)
            }else {
                collectionHeight.constant = cellHeightWidth * CGFloat((collectionArray.count / 3) + 1)
            }
        }
        featureCollection.reloadData()
    }
}


extension EMViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EMCollectionViewCell.identifier, for: indexPath) as? EMCollectionViewCell else {return UICollectionViewCell()}
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellHeightWidth, height: cellHeightWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let EMSelectedCategory = EMSelectCategoryViewController.instance() {
            EMSelectedCategory.delegate = self
            var height = 420.0
            if isIphone() {
                height = 330.0
            }
            let sheetController = SheetViewController(
                controller: EMSelectedCategory,
                sizes: [.fixed(height)])
            sheetController.autoAdjustToKeyboard = true
            sheetController.allowPullingPastMaxHeight = false
            self.present(sheetController, animated: false, completion: nil)
        }
    }
}

extension EMViewController: ExpenseManagementAction {
    func redirectTo(addExpense: Bool) {
        if addExpense {
            if let addExpenseVC = EMAddEditViewController.instance() {
                self.navigationController?.pushViewController(addExpenseVC, animated: true)
            }
        }else {
            if let addExpenseVC = EMExpenseListViewController.instance() {
                self.navigationController?.pushViewController(addExpenseVC, animated: true)
            }
        }
    }
    
    
}
