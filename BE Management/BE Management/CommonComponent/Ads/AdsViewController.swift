//
//  AdsViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class AdsViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    class func instance() -> AdsViewController? {
        return UIStoryboard(name: IdentifierConstant.ads, bundle: nil).instantiateViewController(withIdentifier: AdsViewController.identifier) as? AdsViewController
    }

    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: false)
    }
}
