//
//  NavigationView.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class NavigationView: UIView {
    
    @IBOutlet private (set) weak var leftBarView: UIView!
    @IBOutlet private (set) weak var leftBarImage: UIImageView!
    @IBOutlet private (set) weak var titleLabel: UILabel!
    @IBOutlet weak var rightBarView: UIView!
    
    var leftBarClouser: (()->())?
    var rightBarClouser: (()->())?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        
    }
    
    func loadViewFromNib() -> UIView? {
        let nib = UINib(nibName: IdentifierConstant.navigationView, bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func setUpUI(_ data: NavigationViewModel) {
        titleLabel.text = data.title
        leftBarImage.image = data.isSideMenu ? #imageLiteral(resourceName: "img_sidemenu") :  #imageLiteral(resourceName: "img_back")
        leftBarView.isHidden = !data.isLeftBarShow
        rightBarView.isHidden = !data.isRightBarShow
    }
    
    @IBAction func leftBarAction(_ sender: Any) {
        leftBarClouser?()
    }
    
    @IBAction func rightBarAction(_ sender: Any) {
        rightBarClouser?()
    }
}
