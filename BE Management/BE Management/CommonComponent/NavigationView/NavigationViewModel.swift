//
//  NavigationViewModel.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import Foundation

struct NavigationViewModel {
    let title: String?
    var isSideMenu: Bool = false
    var isLeftBarShow: Bool = true
    var isRightBarShow: Bool = false
}
