//
//  AlertViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit


class AlertViewController: BaseViewController {

    @IBOutlet weak var firstButtonView: UIView!
    @IBOutlet weak var firstButtonTitle: UILabel!
    @IBOutlet weak var secondButtonTitle: UILabel!
    @IBOutlet weak var secondButtonView: UIView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var model: AlertModel?
    var firstButtonClouser: ((AlertType?)->())?
    var secondButtonClouser: ((AlertType?)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    class func instance() -> AlertViewController? {
        return UIStoryboard(name: IdentifierConstant.alert, bundle: nil).instantiateViewController(withIdentifier: AlertViewController.identifier) as? AlertViewController
    }
    
    private func setUI() {
        iconImage.image = model?.image
        titleLabel.text = model?.title
        descriptionLabel.text = model?.description
        firstButtonTitle.text = model?.firstButtonTitle
        secondButtonTitle.text = model?.secondButtonTitle
        secondButtonView.isHidden = model?.secondButtonTitle == nil
    }
    
    @IBAction func firstButtonAction(_ sender: Any) {
        if firstButtonClouser != nil {
            self.dismiss(animated: false)
            firstButtonClouser!(model?.type)
        }
    }
    
    @IBAction func secondBuuttonAction(_ sender: Any) {
        if secondButtonClouser != nil {
            self.dismiss(animated: false)
            secondButtonClouser!(model?.type)
        }
    }
}

