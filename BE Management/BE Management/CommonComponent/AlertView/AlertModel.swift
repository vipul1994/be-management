//
//  AlertModel.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import Foundation
import UIKit

enum AlertType {
    case homeShareUs
    case homeRateUs
    case homeSupport
    case sideMenuPrivacy
    case sideMenuDelete
    case sideMenuPinSet
}

struct AlertModel {
    let image: UIImage?
    let type: AlertType?
    let title: String?
    let description: String?
    let firstButtonTitle: String?
    let secondButtonTitle: String?
}
