//
//  BaseViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit
import SJSwiftSideMenuController

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func setOrientation(stackView: UIStackView) {
        if isLandscapeOrientation() {
            stackView.axis = .horizontal
        }else {
            stackView.axis = .vertical
        }
    }
    
    func showSideMenu() {
        SJSwiftSideMenuController.showLeftMenu()
    }
    
    func isLandscapeOrientation() -> Bool {
        UIScreen.main.bounds.size.width > UIScreen.main.bounds.size.height
    }
    
    func isIphone() -> Bool {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return true
        }else {
            return false
        }
    }
}
