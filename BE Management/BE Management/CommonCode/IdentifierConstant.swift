//
//  IdentifierConstant.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import Foundation

struct IdentifierConstant {
    static let navigationView = "NavigationView"
    static let signIn = "SignIn"
    static let pinCode = "Pincode"
    static let password = "Password"
    static let profile = "Profile"
    static let home = "Home"
    static let alert = "Alert"
    static let support = "Support"
    static let ads = "Ads"
    static let sideMenu = "SideMenu"
    static let currency = "Currency"
    static let language = "Language"
    static let budgetHistory = "BudgetHistory"
    static let walktrough = "Walktrough"
    static let savings = "Savings"
    static let editProfile = "EditProfile"
    static let changePassword = "ChangePassword"
    static let budgetFollowUp = "BudgetFollowUp"
    static let budgetRebalancing = "BudgetRabalancing"
    static let createBudget = "CreateBudget"
    static let expenseManagement = "ExpenseManagement"
    static let premium = "Premium"
}

struct NavigationTitle {
    static let signIn = "SignIn"
    static let pinCode = "Pincode"
    static let password = "Password"
    static let confirmPassword = "Confirm Password"
    static let profile = "Setup Profile"
    static let home = "Home"
    static let support = "Support"
    static let currency = "Currency"
    static let language = "Language"
    static let budgetHistory = "Budget history & Receipts"
    static let editProfile = "Your Profile"
    static let changePassword = "Change Password"
    static let budgetFollowUp = "Budget Follow Up"
    static let budgetRebalancing = "Budget Rebalancings"
    static let setUpBudget = "Setup Budget"
    static let expenseManagement = "Expense Management"
    static let addExpense = "Add Expense"
    static let expenseList = "Expense List"
    static let editExpense = "Edit Expense"
}

