//
//  GlobalUtility.swift
//  BE Management
//
//  Created by Vipul  on 19/05/23.
//

import Foundation
import UIKit

@objc class GlobalUtility: NSObject {
    
    var isAdsShown = false
    
    static let shared: GlobalUtility = {
        let instance = GlobalUtility()
        return instance
    }()
    
    func currentTopViewController() -> UIViewController {
        var topVC: UIViewController? = AppSingleton.appDelegate.window?.rootViewController
        while ((topVC?.presentedViewController) != nil) {
            topVC = topVC?.presentedViewController
        }
        return topVC!
    }
}
