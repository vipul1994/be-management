//
//  UIResponder+Extension.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import Foundation
import UIKit

extension UIResponder {
    static var identifier: String {
        return "\(self)"
    }
}
