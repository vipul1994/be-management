//
//  AppSingeltone.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import Foundation
import UIKit

struct AppSingleton {
    static let appDelegate = UIApplication.shared.delegate  as? AppDelegate ?? AppDelegate()
    static let appName = "BE Management"
}

extension UIDevice {
    static var isIphone: Bool {
        return self.current.userInterfaceIdiom == .phone
    }
    static var isLandscap: Bool {
        return self.current.orientation.isLandscape
    }
}

