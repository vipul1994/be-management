//
//  ViewController.swift
//  BE Management
//
//  Created by Vipul  on 12/05/23.
//

import UIKit

class ViewController: BaseViewController {

    @IBOutlet weak var stackView: UIStackView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        setUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isIphone() {
            setOrientation(stackView: stackView)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            if let VC = WalktroughViewController.instance() {
                self.navigationController?.pushViewController(VC, animated: true)
            }
        })
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
    }
    
    private func setUI() {
        if isIphone() {
            setOrientation(stackView: stackView)
        }
    }

}

